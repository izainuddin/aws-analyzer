import { Component } from '@angular/core';
import { ApiService } from './api.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],
  providers: [ApiService]
})
export class AppComponent {
  title = 'awsApp';
  feedback = "";

  constructor(private api: ApiService){
    this.getFeedbacks();
  }
  
  getFeedbacks = () => {
    this.api.getAllFeedbacks().subscribe(
      data => {
        console.log(data)
        this.feedback =data;
      },
      error => {
          console.log(error)
      }
      
    )
  }
}
