# Aws Analyzer

AWS Analyzer with Django and Angular

Description:

	A web tool to evaluate the performance of Amazon Virtual Machine (with EC2 instances). On the frontend, user perform several operations such as add, multiply, divide, upload image to Amazon S3, a stress test etc. Web tool provides a resulting log on the webpage that shows the performance of the AWS cloud (using Amazon Cloud watch).
	
	A regression POST function to overlead is also added to the frontend. User can click on it and the log shows how well has AWS ELB has performed. 

Technology used:

	Django (Python) --> Backend
    Angular (Typescript) --> Frontend
	Docker (Containerization)
    AWS Cloud